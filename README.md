# frame-miniGame

# version

Cocos Creator 2.4.4

# Introduction



![](https://gitee.com/yanglingfeng37/my-image/raw/master/img/content.png)

## **1.api**

1.1 微信api：提示、模拟对话框、分享及判断、按钮授权、常亮、震动、小程序跳转、激励视频、原生广告和微信广告（间隔刷新，计数销毁）、Cocos坐标转微信坐标。

## **2.components**

A*组件、Mask橡皮擦组件

## **3.core**

### 3.1 design-pattern

FactoryMethod：简单工厂类

Singleton：单例模式类

### 3.2 manager

AssetMgr：资源管理类，继承Singleton类：bundle加载、分帧加载。

AudioMgr：声音管理类，继承Singleton类：实现音乐/音效的播放暂停停止恢复和音频大小、读取本地音乐设置。

EventMgr：事件管理者，继承Singleton类：事件监听的注册，取消，发射，移除。

PoolMgr：对象池管理类，继承Singleton类。

StorageMgr：本地数据管理类，继承Singleton类。

ViewMgr：View管理类，继承Singleton类：管理view下的面板虚基类的子类。

```typescript
//打开首页界面，然后销毁加载页。
//参数：预制体的脚本名，父节点层级，需要传递的参数，回调函数。
ViewMgr.getInstance().openView(HomeView, Layer.viewLayer, null, () => {
 			//销毁加载页
            if (this.launchView) {
                this.launchView.destroy()
                this.launchView = null;
            }
        })
```

### 3.3 view

采用一场景多预制体的方式，Canvas下有子节点gameLayer（游戏层）、viewLayer（界面层）、topLayer（弹窗层）、adLayer（广告层），根据需要将预制体实例化加载到指定的节点"层"。

**只需要明白调用ViewMgr成员方法，创建ViewPanel的子类并重写逻辑即可。**

## 

ViewBase：View基类，继承cc.Component，注册/移除对象点击事件、根据字符串灵活调用cc.find 和getChildByName。



ViewLayer：初始化canvas下的子节点。



ViewPanel：面板虚基类，继承ViewBase，需要子类重写方法。

成员函数：

initView（面板初始化，在__preload中调用，所以会在onLoad和Start之前调用）

show（通过ViewMgr的openView方法自动调用，每次显示界面都调用，可传递参数）、

hide（通过UIMgr的closeUI方法自动调用，每次隐藏界面都调用）、

playIn（界面开场动画）、playOut（界面关闭动画）、

onDestroy（销毁面板，并销毁父类）

## **3.data**

### 3.1 const

存放常量

AudioConst 音频名字管理

EventConst 事件名字管理

StroageConst 本地存储key值管理

### 3.2

GameSetting 配置类，读取后台广告、导出配置。

GlobalValue 全局变量类

UserData 用户数据类

## 4.declarations

微信声明文件

## 5.extensions

拓展

FrameLog：用apply借用cc.log的函数实现自己的log。

## 6.net

NetBase：数据库请求。

WxNet：微信登录、设置接口、玩家登录。

## 7.utils

ArrayUtil：动态申请二维数组、数组乱序、数组克隆、数组求和。

DateUtil：判断两个utc时间戳是否同一天、时间戳生成、秒数转分秒格式、获取两个时间段的秒数、年月分差计算、版本号比较。

LoadUtil：加载图片。

MathUtil：随机范围内的数（浮点型/整型）、随机范围内的数（整型不重复）、数组随机一个单元、弧度制和角度值互相转换、获取两点间弧度、获取两点间旋转角度（顺时针）、获取两点间距离、精确到小数点后多少位（舍尾）、贝塞尔曲线。

NodeUtil：节点坐标转换、销毁节点下的子节点。

NumberUtil：科学计数法转换。

ObjectUtil：判断指定的值是否为对象、深拷贝。

StringUtil：过滤掉特殊符号、判断一个字符串是否包含中文等。