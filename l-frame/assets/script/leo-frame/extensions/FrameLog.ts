import GlobalValue from "../data/GlobalValue";



const {ccclass, property} = cc._decorator;

@ccclass
export default class FrameLog{

    public static log(...args: any[]): void {
        if (GlobalValue.IS_DEBUG) {
            console.log.apply(cc.log, args);
        }
    }

    public static warn(...args: any[]): void {
        if (GlobalValue.IS_DEBUG) {
            console.warn.apply(cc.warn, args);
        }
    }

    public static error(...args: any[]): void {
        if (GlobalValue.IS_DEBUG) {
            console.error.apply(cc.error, args);
        }
    }
}
