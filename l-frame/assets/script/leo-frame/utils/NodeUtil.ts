
const { ccclass, property } = cc._decorator;

@ccclass
export default class NodeUtil {

    static transformPosition(fromNode: cc.Node, toNode: cc.Node) {
        return toNode.convertToNodeSpaceAR(fromNode.parent.convertToWorldSpaceAR(fromNode.position));
    }

    static destroyChildren(node: cc.Node) {
        if (node.childrenCount <= 0) return
        for (let one of node.children) {
            one.destroy();
        }
    }

    static nodeMoveTo(node: cc.Node, time: number, pos: number, cb = () => { }) {
        cc.tween(node)
            .to(time, { x: pos })
            .call(() => {
                cb && cb()
            })
            .start()
    }

}
