
export default class DateUtil {

    /**
     * 比较两个utc时间戳是否为同一天
     */
    public static compare = (utc1, utc2) => {
        let d1 = new Date()
        let d2 = new Date()
        d1.setTime(utc1)
        d2.setTime(utc2)
        console.log("compare", d1, d2)
        if (d1.getMonth() == d2.getMonth() && d1.getDate() == d2.getDate())
            return true;
        return false
    }


    /**
   * 时间戳生成
   * 指定天数 num: 1时为明天，-1为昨天，以此类推
  */
    public static timeFormat(num = 0) {
        let appoint: any, month: string, day: string, hour: string, minute: string, second: string, date: string;
        if (num > 0) {
            appoint = new Date(new Date().getTime() + (num * 24 * 3600 * 1000));
        } else {
            appoint = new Date(Number(new Date()) - (num * 24 * 3600 * 1000));
        }
        month = ('0' + (appoint.getMonth() + 1)).slice(-2);
        day = ('0' + appoint.getDate()).slice(-2);
        hour = ('0' + appoint.getHours()).slice(-2);
        minute = ('0' + appoint.getMinutes()).slice(-2);
        second = ('0' + appoint.getSeconds()).slice(-2);
        date = `${appoint.getFullYear()}/${month}/${day} ${hour}:${minute}:${second}`;
        return date;
    }

    /**
     * 将秒数换成时分秒格式
     * @param {number} value 秒数
     */
    public static secondFormat(value: number) {
        let second: string | number = Math.floor(value),
            minute: string | number = 0,
            hour: string | number = 0;
        // 如果秒数大于60，将秒数转换成整数
        if (second > 60) {
            // 获取分钟，除以60取整数，得到整数分钟
            minute = Math.floor(second / 60);
            // 获取秒数，秒数取佘，得到整数秒数
            second = Math.floor(second % 60);
            // 如果分钟大于60，将分钟转换成小时
            if (minute >= 60) {
                // 获取小时，获取分钟除以60，得到整数小时
                hour = Math.floor(minute / 60);
                // 获取小时后取佘的分，获取分钟除以60取佘的分
                minute = Math.floor(minute % 60);
            }
        }
        // 补位
        hour = hour < 10 ? "0" + hour : "" + hour;
        minute = ('0' + minute).slice(-2);
        second = ('0' + second).slice(-2);
        return { hour, minute, second };
    }

    /**
     * 获取两个时间段的秒数
     * @param {Date} now 对比的时间
     * @param {Date} before 之前的时间
     */
    public static getSecond(now: string, before: string) {
        return (Number(new Date(now)) - Number(new Date(before))) / 1000;
    }

    /**
    * 年份差
    * @param {Date} now 现在时间
    * @param {Date} before 之前时间
    */
    public static getYears(now: Date, before: Date) {
        let d1 = new Date(now)
        let d2 = new Date(before)
        return (d1.getFullYear() - d2.getFullYear())

    }
    /**
    * 月份差
    * Date对象用1表示一个月中的第一天，但用0表示一年中的第一个月。
    * 输入年份数据后，将月份减1再进行月份天数计算。
    * @param {Date} now 现在时间
    * @param {Date} before 之前时间
    */
    public static getMonths(now: Date, before: Date) {
        let d1 = new Date(now)
        let d2 = new Date(before)
        let dy = this.getYears(d1, d2)
        let dm = Math.abs((d1.getMonth() - d2.getMonth()))
        return (dy * 12 + dm)
    }
    /**
        * 天数差
        * @param {Date} now 现在时间
        * @param {Date} before 之前时间
        */
    public static getDays(now: Date, before: Date) {
        let dn1 = new Date(now.getFullYear(), now.getMonth(), now.getDate())
        let db1 = new Date(before.getFullYear(), before.getMonth(), before.getDate())
        return ((dn1.getTime() - db1.getTime()) / 86400000)
    }
    /**
        * 小时差
        * @param {Date} now 现在时间
        * @param {Date} before 之前时间
        */
    public static getHours(now: Date, before: Date) {
        let dn1 = new Date(now.getFullYear(), now.getMonth(), now.getDate())
        let db1 = new Date(before.getFullYear(), before.getMonth(), before.getDate())

        let dd = (dn1.getTime() - db1.getTime()) / 86400000 + (dn1.getTime() - db1.getTime()) % 86400000
        return (dd * 24)
    }
    /**
        * 分钟差
        * @param {Date} now 现在时间
        * @param {Date} before 之前时间
        */
    public static getMins(now: Date, before: Date) {
        let dn1 = new Date(now.getFullYear(), now.getMonth(), now.getDate())
        let db1 = new Date(before.getFullYear(), before.getMonth(), before.getDate())
        return Math.floor((dn1.getTime() - db1.getTime()) / 86400000 * 24 * 60);
    }

    /**
     * 
     * 版本号比较 v1==v2 返回0 v1>v2 返回1 v2>v1 返回-1
     * @param v1 
     * @param v2 
     */
    public static compareVersion(v1, v2) {
        v1 = v1.split('.')
        v2 = v2.split('.')
        const len = Math.max(v1.length, v2.length)

        while (v1.length < len) {
            v1.push('0')
        }
        while (v2.length < len) {
            v2.push('0')
        }
        for (let i = 0; i < len; i++) {
            const num1 = parseInt(v1[i])
            const num2 = parseInt(v2[i])

            if (num1 > num2) {
                return 1
            } else if (num1 < num2) {
                return -1
            }
        }
        return 0
    }

    // compareVersion(version) {
    //     /** 当前版本号 */
    //     let v1 = this.system['SDKVersion'].split('.');
    //     let v2 = version.split('.');
    //     const len = Math.max(v1.length, v2.length);
    //     while (v1.length < len) {
    //         v1.push('0');
    //     }
    //     while (v2.length < len) {
    //         v2.push('0');
    //     }
    //     for (let i = 0; i < len; i++) {
    //         const num1 = Math.floor(Number(v1[i]));
    //         const num2 = Math.floor(Number(v2[i]));
    //         if (num1 > num2) {
    //             return 1;
    //         } else if (num1 < num2) {
    //             return -1;
    //         }
    //     }
    //     return 0;
    // }
}
