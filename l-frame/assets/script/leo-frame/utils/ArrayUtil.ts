import MathUtil from "./MathUtil";

export default class ArrayUtil {
    /**
     * 动态申请二维数组，元素初始化为0。
     * @param row 行
     * @param column 列 
     */
    public static new(row: number, column: number): number[] {
        let arr: any
        for (let i = 0; i < row; i++) {
            arr[i] = new Array()
            for (let j = 0; j < column; j++) {
                arr[i][j] = 0;
            }
        }
        return arr;
    }
    /**
     * 
     * @param arr 数组乱序
     */
    public static shuffle(arr: any[]) {
        if (arr == null || arr.length == 0)
            return arr;
        let length = arr.length;
        for (let i = 0; i < length; i++) {
            let j = MathUtil.randomRangeInt(0, length - 1)
            let temp = arr[i];
            arr[i] = arr[j]
            arr[j] = temp;
        }
        return arr;
    }

    public static clone<T>(arr: T[]) {
        let ary: T[] = []
        arr.forEach((value, index) => {
            ary.push(value)
        })
        return ary;
    }

    public static sum(arr: number[]) {
        var s = 0;
        arr.forEach(function (val) {
            s += val;
        }, 0);
        return s;
    };

}
