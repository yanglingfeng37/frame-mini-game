
export default class NumberUtil {

    public static format(value) {
        //格式化 金币数据
        let v = value + ""
        if (v.indexOf("e") != -1)
            value = this.convertNum(value)
        else {
            value = Math.floor(value) + ""
        }
        // GameLog.log("数值格式化：" + value)

        let res = value + ""
        if (res.length > 21)
            res = res.substring(0, res.length - 21) + "." + res.substr(res.length - 21, 2) + "B"
        else if (res.length > 18)
            res = res.substring(0, res.length - 18) + "." + res.substr(res.length - 18, 2) + "E"
        else if (res.length > 15)
            res = res.substring(0, res.length - 15) + "." + res.substr(res.length - 15, 2) + "P"
        else if (res.length > 12)
            res = res.substring(0, res.length - 12) + "." + res.substr(res.length - 12, 2) + "T"
        else if (res.length > 9)
            res = res.substring(0, res.length - 9) + "." + res.substr(res.length - 9, 2) + "G"
        else if (res.length > 6)
            res = res.substring(0, res.length - 6) + "." + res.substr(res.length - 6, 2) + "M"
        else if (res.length > 3)
            res = res.substring(0, res.length - 3) + "." + res.substr(res.length - 3, 2) + "K"
        return res;
    }

    private static convertNum(amount) {
        // 判断是否科学计数法,是则进行转换
        let num = 0;
        let result = [];
        amount = new String(amount);
        if ((amount.indexOf('E') != -1) || (amount.indexOf('e') != -1)) {
            (amount.indexOf('E') != -1) ? num = amount.indexOf('E') : num = amount.indexOf('e');
            let decimal = amount.substr(0, num);
            // devide the sentice efficial number
            let decArr = decimal.split('.', 2);
            // total 10 power
            let power = amount.substr(num + 1, amount.length);
            // symbol + -
            let symbol = power.substr(0, 1);
            power = power.substr(1, power.length);
            power = power * 1.0;
            if ('+' == symbol) {
                // the number of 0 is power - decArr[1]
                power = power - decArr[1].length;
                // the return data

                //while 改成for
                for (let i = power; i > 0; i--) {
                    result.unshift(0);
                    power -= 1;
                }

                // while (power > 0) {
                //     result.unshift(0);
                //     power -= 1;
                // }
                result.unshift(decArr[1] * 1.0);
                result.unshift(decArr[0] * 1.0);
            } else {
                power = power - decArr[1].length;
                result.push('.');
                //while 改成for

                for (let i = power; i > 0; i--) {
                    result.unshift(0);
                    power -= 1;
                }

                // while (power > 0) {
                //     result.push(0);
                //     power -= 1;
                // }
                result.push(decArr);
            }
        }
        result.join("");
        let res: string = result + '';
        amount = res.replace(/,/g, '');
    }
}
