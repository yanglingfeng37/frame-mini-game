import GlobalValue from "../data/GlobalValue";
import FrameLog from "../extensions/FrameLog";
import WxNet from "./WxNet";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NetBase {
    public host = "";
    public version: string = GlobalValue.GAME_CONF.version
    public appid: string = GlobalValue.GAME_CONF.appid
    private _sessionId: string = ""
    private _date: number = 0
    public request(object: { url: string, method?: string, host?: string, header?: any, data?: any, isLoginURL?: boolean, complete?: Function }) {
        FrameLog.log("NetBase - 请求URL", this.host + object.url)
        return new Promise((resolve, reject) => {
            let param = Object.create({})
            param.url = object.host ? object.host + object.url : this.host + object.url;
            param.method = object.method || "GET";
            param.header = object.header || {};
            param.data = object.data || {};

            param.success = (res) => {
                if (res.statusCode == 200) {
                    if (object.isLoginURL) {
                        this.setSessionID(res);
                    }
                    let body = res.data;
                    if (body.code == 200) {
                        resolve(body.data)
                    } else if (body.code == 400) {
                        FrameLog.warn("NetBase - session过期");
                        reject()
                        if (Date.now() - this._date > 60000) {
                            this._date = Date.now()
                            WxNet.getInstance().init()
                        }
                    } else {
                        let errInfo = Object.create({})
                        errInfo.code = body.code;
                        errInfo.msg = body.msg;
                        reject(errInfo)
                        FrameLog.log(object.url + " error: " + body.code + " " + body.msg);
                    }
                }
                else {
                    reject()
                }
            };
            param.fail = () => {
                reject()
                FrameLog.warn(`NetBase - fail - url:${object.url}`,);
            };

            param.complete = () => {
                if (object && object.complete) {
                    object.complete();
                }
            };

            if (param.method == "POST") {
                param.header["Content-Type"] = "application/json";
            }
            param.header.version = this.version;//header添加上版本号,区分接口
            param.header.appid = this.appid;//header 添加appid
            param.header["Cookie"] = "PHPSESSID=" + this._sessionId;
            wx.request(param);
        })

    }
    private setSessionID(res: any) {
        let cookie = res.header["Set-Cookie"] == undefined ? '' : res.header["Set-Cookie"];
        let sessionArr = cookie.match(/PHPSESSID\=(.*)\;/)
        if (sessionArr != null) {
            this._sessionId = sessionArr[1];
        }
        FrameLog.log(`NetBase - setSessionID`, this._sessionId)
    }
}
