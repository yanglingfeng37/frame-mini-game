import Singleton from "../design-pattern/Singleton";

/**
 * 本地数据管理类
 */
export default class StorageMgr extends Singleton{

    public get(key: string) {
        try {
            if (key != null) {
                var result = cc.sys.localStorage.getItem(key);
                if (result) {
                    result = JSON.parse(result);
                }
                return result;
            }
        } catch (err) {
            cc.error(`获取数据${key}失败`,err)
        }
    }

    public  set(key: string, value: any) {
        try {
            if (key != null) {
                return cc.sys.localStorage.setItem(key, JSON.stringify(value));
            }
        } catch (err) {
            cc.error(err);
        }
    }

    public  clear() {
        return cc.sys.localStorage.clear();
    }
    
    public  remove(key: string) {
        if (key != null) {
            return cc.sys.localStorage.removeItem(key);
        }
    }
}

