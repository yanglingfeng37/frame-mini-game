
const { ccclass, property } = cc._decorator;
/**单例模板类 */
@ccclass
export default class Singleton {
    private static instance: Singleton;
    public static getInstance<T extends {}>(this: new () => T): T {
        if (!(<any>this).instance) {
            (<any>this).instance = new this();
        }
        return (<any>this).instance;
    }
}
