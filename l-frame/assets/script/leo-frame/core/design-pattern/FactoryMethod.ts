

interface AbstractProduct {
    createProduct(type?: any): any;
}
const { ccclass, property } = cc._decorator;

@ccclass
export default class FactoryMethod implements AbstractProduct {
    createProduct(type?: any) {
        //创建父类类型对象
        let obj = null;
        switch (type) {
            case "ENEMY_FISH"://根据枚举值分类
                //obj = 调用子类对应的create方法
                break;
        }
        return obj;//返回子类对象
    }


}
