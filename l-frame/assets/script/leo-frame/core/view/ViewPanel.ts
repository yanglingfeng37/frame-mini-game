
import FrameLog from "../../extensions/FrameLog";
import ViewBase from "./ViewBase";




const {ccclass, property} = cc._decorator;

@ccclass
export default abstract class ViewPanel extends ViewBase {

    public static getUrl(): string {
        FrameLog.error("需重写getUrl")
        return ""
    }
    __preload() {
        this.initView()
    }

    /**
     * 
     *  面板初始化,第一次生成的时候调用
     */
    abstract initView(): void;

    /**
     *   
     * 面板显示 每次显示都调用 可以进行相关初始化（View、事件）会在onload，start之前调用
     * @param param 面板显示参数
     */
    abstract show(param: any): void;

    /**
     * 面板隐藏  每次因此都调用 取消 ClientEvent 和 按钮点击监听
     */
    abstract hide(): void;

    /**打开面板动画 */
    abstract playIn();

    /**关闭面板动画 */
    abstract playOut();

    /**
     * 
     */
    onDestroy() {
        // ClientEvent.inst.targetOff(this)
        super.onDestroy()
    }
}
