import FrameLog from "../../extensions/FrameLog";

const { ccclass, property } = cc._decorator;
/**
 * canvas下的子节点
 * 层级大小：gameLayer <  viewLayer < topLayer < adLayer
 */
@ccclass
export default class ViewLayer  {
    public static viewLayer: cc.Node = null;
    public static gameLayer: cc.Node = null;
    public static topLayer: cc.Node = null;
    public static adLayer: cc.Node = null;

    public static init(param: any) {
        this.viewLayer = param.viewLayer;
        this.gameLayer = param.gameLayer
        this.topLayer = param.topLayer;
        this.adLayer = param.adLayer;
    }

}
