

import AudioConst from "../../data/const/AudioConst";
import FrameLog from "../../extensions/FrameLog";
import AudioMgr from "../manager/AudioMgr";


const { ccclass, property } = cc._decorator;

@ccclass
export default class ViewBase extends cc.Component {

    private _touchList: { [key: string]: { target, handler } } = {}
    private _enableList: { [key: string]: { enabled: boolean, isGray: boolean } } = {}

    /**
   * 是否交互 需在target注册onTouch之后
   * @param target 
   * @param v 
   * @param isGray 
   */
    protected setInteractable(target: cc.Node, v: boolean, isGray: boolean = true) {
        if (!target)
            return;
        let button: cc.Button = target.getComponent(cc.Button)
        if (button) {
            button.enableAutoGrayEffect = isGray;
            button.interactable = v
        }
        this._enableList[target.name] = { enabled: v, isGray }
    }

    /**
     * 
     * @param target    点击对象
     * @param handler   触发事件
     * @param sound     播放声音名称
     * @param scale     缩放值
     */
    protected onTouch(target: cc.Node, handler: Function, sound: string = AudioConst.BTN_DEF_CLICK, scale = 0.9, stopEvent = true) {
        if (!target || !handler) {
            FrameLog.error("onTouch - target || handler", target, handler)
            return
        }
        let targetName: string = target.name
        if (this._touchList[targetName] && this._touchList[targetName].target == target) {
            FrameLog.warn("onTouch - touchList已存在", targetName)
            return
        }


        let button = target.getComponent(cc.Button)
        if (scale != 1) {
            if (!button) {
                button = target.addComponent(cc.Button)
                button.transition = cc.Button.Transition.SCALE
                button.zoomScale = scale
            }
        }
        let { enabled, isGray } = this._enableList[target.name] || { enabled: true, isGray: true }
        this.setInteractable(target, enabled, isGray)

        let touchHandler = (event) => {
            if (stopEvent)
                event.stopPropagation()//停止传递当前事件
            if (sound && sound != "") {
                AudioMgr.getInstance().playEffect(sound)
            }
            let { enabled = true } = this._enableList[target.name] || {}
            if (!enabled)
                return;
            handler(event)
        }
        target.on(cc.Node.EventType.TOUCH_END, touchHandler);
        this._touchList[targetName] = { target: target, handler: touchHandler }
    }

    /**
     * 移除对象点击事件
     * @param target 
     */
    protected offTouch(target: cc.Node) {
        if (!target) {
            FrameLog.error("offTouch - target 为空")
            return
        }
        let targetName: string = target.name
        if (this._touchList[targetName]) {
            let touchHandler = this._touchList[targetName].handler;
            target.off(cc.Node.EventType.TOUCH_END, touchHandler)
            delete this._touchList[targetName]
        }
        delete this._enableList[targetName]
    }

    protected clear() {
        for (let key in this._touchList) {
            this.offTouch(this._touchList[key].target)
        }
    }

    onDestroy() {
        this.clear()
    }


    /**
     * 
     * @param path 路径或者名字
     */
    protected getNode(path: string): cc.Node {
        let node: cc.Node = null;
        if (path == "" || !path)
            return null;
        if (path.indexOf("/") != -1) {
            node = cc.find(path, this.node)
        }
        else {
            node = this.node.getChildByName(path)
        }

        if (!node)
            FrameLog.warn("未找到该节点  path=", path)
        return node;
    }
}
