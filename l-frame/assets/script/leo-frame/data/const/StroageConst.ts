
export default class StroageConst {
    //[sessionKey,soundKey] 

    public static readonly userData: string = "userData";
    public static readonly guide: string = "guide";
    public static readonly zdKey: string = "zdKey";//震动设置
    public static readonly propKey: string = "propKey";//提醒道具
    public static readonly playerSkin: string = "playerSkin";//记录视频获得皮肤的次数
    public static readonly favorite: string = "favorite";//记录视频获得皮肤的次数
    public static readonly gold_not: string = "gold_not";//金币不足

    public static readonly MULTIPOINT: string = "multipoint"//记录多点 是否支持多点0否(点击后消失)1是(点击后置后)-1关闭此功能
}
