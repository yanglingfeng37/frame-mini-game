
import Singleton from "../core/design-pattern/Singleton";

const { ccclass, property } = cc._decorator;
/**后台配置类
 * 
 */
@ccclass
export default class GameSetting extends Singleton {

    //#region banner
    /**banner id */
    public ad_bannerID: string[] = [];
    /**banner 自动刷新 */
    public bannerInterval: number = 0;
    /**banner 后台控制刷新 */
    public bannner_refresh_time: number = 0;
    //#endregion  banner
    /**配置分享图 */
    public share_info = [{
        id: 0,
        images: "",
        title: ""
    }];
    /**视频id*/
    public ad_videoID: string[] = []
    /**原生广告id */
    public ad_CustomID: string[] = []
    
    /*默认配置 */
    private _defValue = {}
    public init(param: { [key: string]: any } = null) {
        if (!param)//没有数据，默认配置
            param = this._defValue;
        for (let key in param) {
            if (this.hasOwnProperty(key)) {
                this[key] = param[key];
            }
            //如果需要全部替换，直接赋值
        }
    }
}
