
const { ccclass, property } = cc._decorator;

@ccclass
export default class GlobalValue  {
    /**游戏配置 */
    static readonly GAME_CONF = {
        appid: ``,
        miniId: 0,
        gameName: ``,
        version: "1.0.0",
        channel: `wx`,//wx, oppo, vivo, tt, qq
    }
     // session信息
     public static sessionInfo: any = {
        user_code: null,
        openid: null
    };
    /**是否开启debug */
    static readonly IS_DEBUG = true;
    /**前后台时间戳 */
    static onHideDate: Date = null;
    static onShowDate: Date = null;
    //#region share value Start
    /**是否正在分享 */
    static is_sharing: boolean = false
    /**分享概率判断 */
    static is_share_success: boolean = false
    //#endregion share value End
    /**banner是否刷新 */
    static refreshBanner: boolean = false;
}
