import GameSetting from "../../data/GameSetting";
import GlobalValue from "../../data/GlobalValue";

const { ccclass, property } = cc._decorator;
enum BannerState {
    None = "None",
    Ready = "Ready",
    Show = "Show",
    Hide = "Hide"
}
interface BannerInfo {
    bannerAd: any,
    adUnitId: string
    state: BannerState,
    count?: number
}
/**
 * 微信 - banner
 * 根据后台配置时间进行刷新广告,增加点击率.
 * 根据后台配置显示次数进行销毁广告,增加曝光率.
 * 挂载canvas下即可
 */
@ccclass
export default class WxBanner extends cc.Component {
    public static inst: WxBanner = null;
    protected onLoad() {
        WxBanner.inst = this;
    }
    /**可显示banner对象的容器 */
    private _bannerAds: BannerInfo[] = [];
    /**当前需要显示的banner下标 */
    private _bannerCounter: number = 0;
    /**时间记录 */
    private _timeTemp: number = 0;
    /** 检查的下标*/
    private _indax: number = 0;
    /**初始化banner */
    public init() {
        if (cc.sys.platform != cc.sys.WECHAT_GAME || GameSetting.getInstance().ad_bannerID.length == 0) return;
      
        GameSetting.getInstance().ad_bannerID.forEach((v) => {
            this.create(v);
        })
        // console.log("所有的bannerAd",this.__bannerAds)
    }
    /**创建banner */
    public create(adUnitId: string) {
        let tempWid: number = 100
        if (cc.winSize.height >= 1500) {
            tempWid = cc.winSize.width - 100
        }
        let param: any = {
            adUnitId: adUnitId,
            adIntervals: GameSetting.getInstance().bannerInterval,
            style: {
                left: 0,
                top: cc.view.getFrameSize().height - 100,
                width: tempWid,
            }
        }
        let obj = Object.create({})
        obj = wx.createBannerAd(param)
        let self = this
        if (obj) {
            obj.onError(() => {
                obj = null
            });
            obj.onResize((res) => {
                let sysInfo = wx.getSystemInfoSync();
                let gap = 0;
                if (sysInfo.screenHeight / sysInfo.screenWidth > 1.9) gap = 18;
                obj.style.height = res.height;
                console.log("banner 高度", res.height)
                obj.style.left = sysInfo.screenWidth - res.width >> 1;
                obj.style.top = cc.view.getFrameSize().height - res.height - gap
            })
            obj.onLoad(() => {
                obj.hide()
                self._bannerAds.push({ bannerAd: obj, adUnitId: param, state: BannerState.Hide })
            })
        }
    }
    /**检查banner */
    public check() {
        return this._bannerAds.length > 0
    }
    /**banner控制*/
    public bannerHandler(forceHide: boolean = false) {
        if (forceHide) {//强制隐藏
            this._bannerCounter = 999;
        }
        else {
            if (this._bannerCounter >= this._bannerAds.length) this._bannerCounter = 0;
        }
        //检查banner展示次数，超过特定次数则从显示容器里删除，销毁，重新创建
        this._indax = 0
        while (this._indax <= this._bannerAds.length - 1) {
            if (this._bannerAds[this._indax].count > 5) {
                this._bannerAds[this._indax].bannerAd.destroy()
                this.create(this._bannerAds[this._indax].adUnitId)
                this._bannerAds.splice(this._indax, 1)
            } else {
                this._indax += 1;
            }
        }
        this._bannerAds.forEach((v, i) => {

            if (i === this._bannerCounter) {
                v.bannerAd.show()
                v.state = BannerState.Show
                // console.log("bannerHandler show", this._bannerCounter)
            }
            else {
                v.bannerAd.hide()
                v.state = BannerState.Hide
                // console.log("bannerHandler Hide", this._bannerCounter)
            }
        })
    }
    /**隐藏banner */
    public hide() {
        GlobalValue.refreshBanner = false
        this.bannerHandler(true)
    }
    /**显示banner */
    public async show() {
        if (this.check()) {
            this.bannerHandler(false)
            this._bannerCounter += 1
            GlobalValue.refreshBanner = true
        }
    }

    
    update(dt) {
        if (GlobalValue.refreshBanner) {
            this._timeTemp += dt
            if (this._timeTemp >= GameSetting.getInstance().bannner_refresh_time) {
                this._timeTemp = 0
                this.bannerHandler(false)
                this._bannerCounter += 1
            }
        }

    }
}
