import ViewPanel from "../leo-frame/core/view/ViewPanel";

const {ccclass, property} = cc._decorator;

@ccclass
export default class HomeView extends ViewPanel {
    public static getUrl(): string {
        return "preload/view/HomeView"
    }
    initView(): void {
    }
    show(param: any): void {

    }
    hide(): void {
    }
    playIn() {
    }
    playOut() {
    }

    
}
