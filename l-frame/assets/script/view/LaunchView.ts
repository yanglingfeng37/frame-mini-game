import AssetMgr from "../leo-frame/core/manager/AssetMgr";
import EventMgr from "../leo-frame/core/manager/EventMgr";
import PoolMgr from "../leo-frame/core/manager/PoolMgr";
import EventConst from "../leo-frame/data/const/EventConst";
import FrameLog from "../leo-frame/extensions/FrameLog";
import WxNet from "../leo-frame/net/WxNet";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LaunchView extends cc.Component {

  @property(cc.Sprite)
  private progressBar: cc.Sprite = null;
  @property(cc.Sprite)
  private loadingCircle:cc.Sprite =null;
  protected async start() {
    this.progressBar.fillRange = 0;
    this.loadingCircle.node.opacity = 255;
    await WxNet.getInstance().init()
    this.preload()
  }

  private preload() {
    let promise = [];
    // //加载bundle
    const p1 = new Promise((resolve, reject) => {
      cc.assetManager.loadBundle('bundle', (err, bundle) => {
        if (err) {
          console.log(`loadBundle error`, err)
          reject()
        }
        resolve(null)

      });
    })
    promise.push(p1)

    //加载预加载文件夹
    const p2 = new Promise((resolve, reject) => {
      cc.resources.preloadDir('preload',
        (finish, total, item) => {
          let value = cc.misc.clamp01(finish / total);
          this.progressBar.fillRange = value;
          if (value >= 1) {
            resolve(null);
          }
        },
        (err, items) => {
          if (err) {
            reject(null);
          }
        });
    })
    promise.push(p2)

    //加载完成，发送事件。
    Promise.all(promise).then(() => {
      EventMgr.getInstance().emit(EventConst.GAME_LAUNCH);
    });
  }


}
