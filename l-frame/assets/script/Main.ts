import AudioMgr from "./leo-frame/core/manager/AudioMgr";
import EventMgr from "./leo-frame/core/manager/EventMgr";
import ViewMgr from "./leo-frame/core/manager/ViewMgr";
import ViewLayer from "./leo-frame/core/view/ViewLayer";
import EventConst from "./leo-frame/data/const/EventConst";
import GameSetting from "./leo-frame/data/GameSetting";
import UserData from "./leo-frame/data/UserData";
import HomeView from "./view/HomeView";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Main extends cc.Component {
    //加载页
    private launchView: cc.Node;

    //游戏初始化
    onLoad() {
        //初始化节点
        this.launchView = this.node.getChildByName("LaunchView")
        let gameLayer = this.node.getChildByName("gameLayer")
        let viewLayer = this.node.getChildByName("viewLayer")
        let topLayer = this.node.getChildByName("topLayer")
        let adLayer = this.node.getChildByName("adLayer")
        ViewLayer.init({ gameLayer, viewLayer, topLayer, adLayer })
        //初始化用户数据
        UserData.getInstance().init()
        //初始化游戏设置，从后台读取配置
        GameSetting.getInstance().init()
        //初始化声音
        AudioMgr.getInstance().init()
        //注册预加载事件
        EventMgr.getInstance().on(EventConst.GAME_LAUNCH, () => {
            this.appStart()
        }, this)
      
    }

    //加载完成，进入游戏。
    appStart() {
        //打开首页界面
        ViewMgr.getInstance().openView(HomeView, ViewLayer.viewLayer, null, () => {
            if (this.launchView) {
                this.launchView.destroy()
                this.launchView = null;
            }
        })
    }


}
